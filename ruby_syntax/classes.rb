module Animals
  class Animal
    attr_accessor :age, :weight

    def initialize(age = 0, weight = 1)
      raise 'Cannot init animal'
    end

    def eat
      @weight += 1
      puts "current weight: #{@weight}"
      @weight
    end

    def show_total_amount
      puts "Total amount of animals: #{@@animals_count}"
    end
  end

  class Rodent < Animal
    def initialize(age = 0, weight = 3)
      puts "initializing...age: #{age} weight: #{weight}"
      @age = age
      @weight = weight
      @@animals_count ||= 0
      @@animals_count += 1
    end

    def eat(food)
      puts 'hello'
      @weight = super() + 1
      puts 'world'
    end
  end

  class SwimmingError < ::StandardError
  end

  module SwimmingAbility
    WEIGHT_LOST_PER_DISTANCE_COEFF = 0.01

    def swim(distance)
      puts 'swimming...'
      raise SwimmingError.new('non-valid distance') unless distance.is_a?(Integer)
      @weight -= distance * WEIGHT_LOST_PER_DISTANCE_COEFF
    rescue SwimmingError => e
      puts "Swimming error #{e.message}\n #{e.backtrace.join("\n")}"
    rescue => e
      puts "Exception occurred #{e.message}\n #{e.backtrace.join("\n")}"
    end
  end

  class Capybara < Rodent
    include ::Animals::SwimmingAbility
    def initialize(age = 0, weight = 100)
      super
    end
  end

  class Beaver < Rodent
    include SwimmingAbility

    private

    def dig
      puts 'digging...'
    end
  end
end

module Automation
  class Capybara
  end
end

module Util
  def self.foo
    puts 'bar'
  end
end
